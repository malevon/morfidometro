<?php
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');

if (isset($_POST['com'])) {

    try {
        include 'database/conexion.php';

        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);
        $consulta = $cnx->prepare("INSERT INTO comida (comida2) VALUES (:comida)");
        $resultado = $consulta->execute(array(
            'comida' => $_POST['com']
        ));

        $id_comida = $cnx->lastInsertId();

        if (isset($_POST['id_comida'])) {
            $id_comida = $_POST['id_comida'];
        }
    } catch (ExceptionType $e) {
        //⋮ handle the exception
        'Unable to connect to the database server: ' . $e;
    }
}
echo "Comida agregada gracias...";
?>
<html>
    <body>
        <form action="morfi3.php" method="post">
            Agregado: <input type="text" name="agr" /><br><br>
            Cantidad en gr: <input type="number" name="can" /><br><br>
            Costo $: <input type="number" name="cos" /><br><br>
            Por persona: <input type="number" name="pper" /><br><br>
            <input type="hidden" value="<?php echo $id_comida; ?>" name="id_comida" >
            <input type="submit" name="submit"  value="Otro">
        </form>
    </body>
</html>
