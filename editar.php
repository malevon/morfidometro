<?php
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');

include 'database/conexion.php';

// traigo el valor del parámetro pasado en el query string
// isset() es una función PHP usada para verificar si un valor existe
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: Registro no encontrado.');
$flag = isset($_GET['flag']) ? $_GET['flag'] : null;
try {

    $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);
    /*
     * preparamos la sentencia de selección dependiendo del flag,
     * si viene con un valor y es igual a m quiere decir que vamos a editar el nombre de la comida
     * si vale i vamos a editar los ingredientes
     * si no tiene valor no hacemos nada
     */
    if ($flag === 'm') {
        $consulta = $cnx->prepare("SELECT * FROM comida where id = :id ");
    } elseif ($flag === 'i') {
        $consulta = $cnx->prepare("SELECT * FROM agregado WHERE id = :id");
    }

    $resultado = $consulta->execute(array(
        ':id' => $id
    ));


    // Este es el primer signo en el query string 
    $consulta->execute();
    $row = $consulta->fetch(PDO::FETCH_ASSOC);


    if (!isset($row)) {
        echo "Por favor selecciona un valor válido";
        return; //termino la ejecución 
    } else {
        //habilito el formulario para modificar el campo
        // @todo: formulario para modificar el valor del campo
//        var_dump($row); // array(2) { ["id"]=> string(2) "15" ["comida2"]=> string(5) "pizza" }
        if ($flag === "m") {
            include 'parciales/_editar_comida.php';
        } elseif ($flag === 'i') {
            include 'parciales/_editar_agregado.php';
        }

    }
} catch (Exception $e) {
    //⋮ handle the exception
    //echo "hubo un error de cnx";
}
