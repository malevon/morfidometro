<?php
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');


#Isset comprueba que las variables no estén vacías; resuelvo con el operador ternario (condicion ? verdadero :  falso)
$agr = isset($_POST['agr']) ? $_POST['agr'] : null;
$can = isset($_POST['can']) ? $_POST['can'] : null;
$cos = isset($_POST['cos']) ? $_POST['cos'] : null;
$per = isset($_POST['pper']) ? $_POST['pper'] : null;
$id_comida = isset($_POST['id_comida']) ? $_POST['id_comida'] : null;

if (isset($agr, $can, $cos, $per)) {

    try {
        include 'database/conexion.php';

        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);
        $consulta = $cnx->prepare("INSERT INTO comida (comida2) VALUES (:agregado, :cantidad, :costo, :per, :id_comida)");

        $resultado = $consulta->execute(array(
            'agregado' => $agregado,
            'cantidad' => $can,
            'costo' => $cos,
            'per' => $per,
            'id_comida' => $id_comida
        ));
    } catch (ExceptionType $e) {
        //⋮ handle the exception
        'Unable to connect to the database server: ' . $e;
    }
}
?>
<html>
    <body>

        <form action="morfi2.php" method="post">
            <input type="hidden" value="<?php echo $id_comida; ?>" name="id_comida" >
            <input type="submit" name="submit"  value="Agregar Otro">
        </form>
        
        
        <a href="index.php">HOME</a>
    </body>
</html>

