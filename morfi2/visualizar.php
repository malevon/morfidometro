
<title>Publicaciones</title>
</head>
<body>


    <form action="morfi.php" method="post">
        <input value="volver" type="submit" />
    </form>
    <form action="visualizar2.php" method="post">
        <input value="proximo" type="submit" />
    </form>

    <table border="1" cellpadding="1" cellspacing="1">

        <tbody>


            <?php
            include 'database/conexion.php';
            try {
                
                $pdo = new PDO(DB_INFO, DB_USER, DB_PASS);

                # Para que genere excepciones a la hora de reportar errores.
                $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                
                
                $consulta = "SELECT * FROM comida";
                $preparo = $pdo->prepare($consulta);

                $resultado = $preparo->execute();
                $resultado->fetchAll();
                
            } catch (Exception $e) {
                //⋮ handle the exception
                //echo "hubo un error de cnx";
            }

            if (isset($resultado)) {
                ?>
                <tr>
                    <td>&nbsp;ID&nbsp;</td>
                    <td>&nbsp;Morfi&nbsp;</td>
                </tr>
                    <?php
                    while ($row = $preparo->fetch(PDO::FETCH_ASSOC)) {
                        ?>
                        <tr>
                            <td>
                                <a href="visualizar2.php/?comida=<?php echo $row['id']; ?>">Ver mas</a>
                            </td>
                            <td><?php echo $row['comida2']; ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr><td colspan="2">No hay resultados</td></tr>
                    <?php
                }
                ?>
        </tbody>
    </table>
