<?php
// Si no existe el archivo para la conexion no muestra nada.
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');

if (isset($_POST['com'])) {

    try {
        //⋮ do something risky
        include 'database/conexion.php';

        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);

        $consulta = $cnx->prepare("INSERT INTO comida (comida2) VALUES (:comida)");

        $resultado = $consulta->execute(array(
            'comida' => $_POST['com']
        ));

        $id_comida = $cnx->lastInsertId();

        if (isset($_POST['id_comida'])) {
            $id_comida = $_POST['id_comida'];
        }
    } catch (ExceptionType $e) {
        //⋮ handle the exception
        'Unable to connect to the database server: ' . $e;
    }

    echo "Comida agregada gracias...";
}
?>
<html>
  <body>
    <h1>Morfidometro</h1>
    <form action="morfi2.php" method="post">
      Pedido: <input type="text" name="com" /><br>
      <input type="submit" value="Agregar"/>
    </form>
    <form action="visualizar.php" method="post">
      <input type="submit" value="Ver"/>
    </form>
  </body>
</html>
