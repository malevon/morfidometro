# Morfidometro

Sirve para poder estimar cuánto nos costará un plato de comida con sus ingredientes dependiendo de la cantidad de comenzales. Forma parte de un taller de programación de la fundación [Por el Arte hacia la vida](https://www.fundacionarteyvida.org.ar).  


## Cómo comenzar

Necesitarás una base de datos MYSQL, en create-schema.sql está el script de creación de la base de datos y las tablas.

Deberás copiar la carpeta database con sus contenidos al root del sitio y modificarle los parámetros de conexión.

### Prerequisitos

Para instalarla necesitarás PHP 7, APACHE 2 y MySQL.


## Running the tests

Aún y lamentablemente no estamos implementando tests.

## Built With

* [PHP](http://php.net) - The web framework used
* [MYSQL](https://maven.apache.org/) - Dependency Management

## Authors

* **Malevon** - *Initial work* - [Malevon](https://github.com/malevon)
* **María Noel** - *Initial work* - [Bacodesign](https://github.com/bacodesign)

Existen [contributors](https://github.com/morfidometro/contributors) que colaboraron con nosotros  .

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* La idea de este proyecto es ir aprendiendo a través de la experiencia y de meter código.
* Nos inspiró el amor por el conocimiento (ser tan ñoños).
