-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2018 at 01:06 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `morfidometro`
--

-- --------------------------------------------------------

--
-- Table structure for table `agregado`
--

CREATE TABLE IF NOT EXISTS `agregado` (
  `comida2` int(10) NOT NULL,
  `agregados` varchar(30) NOT NULL,
  `cant` int(10) NOT NULL,
  `costos` int(10) NOT NULL,
  `ppersona` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agregado`
--

INSERT INTO `agregado` (`comida2`, `agregados`, `cant`, `costos`, `ppersona`) VALUES
(0, '', 0, 0, 0),
(0, 'chori de cerdo', 500, 65, 4),
(0, 'pan', 600, 35, 4),
(0, 'queso', 450, 120, 5),
(0, 'salame', 450, 80, 4),
(0, 'jamon', 450, 68, 4),
(0, 'cantinpalo', 450, 70, 4),
(0, 'aceitunas', 300, 34, 4),
(0, 'pan', 500, 30, 4),
(0, 'milanesas', 500, 120, 5),
(0, 'pan', 500, 20, 5),
(0, 'milanesas', 1000, 200, 5),
(0, 'tomate', 1000, 30, 5),
(0, 'choclo', 300, 30, 5),
(0, 'queso', 200, 30, 5);

-- --------------------------------------------------------

--
-- Table structure for table `comida`
--

CREATE TABLE IF NOT EXISTS `comida` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `comida2` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `comida`
--

INSERT INTO `comida` (`id`, `comida2`) VALUES
(1, 'panchos'),
(2, 'pizzeada'),
(6, 'chorizeada'),
(7, ''),
(8, 'picada'),
(9, ''),
(10, ''),
(11, ''),
(12, ''),
(13, 'milaneseada'),
(14, ''),
(15, 'sandwicheada'),
(16, ''),
(17, ''),
(18, 'minta'),
(19, ''),
(20, 'chancho');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
