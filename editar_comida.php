<?php
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');

if ($_POST) {
    try {

        // write update query
        // in this case, it seemed like we have so many fields to pass and
        // it is better to label them and not use question marks
        $query = "UPDATE comida 
                    SET comida2=:nombre 
                    WHERE id = :id";

        $stmt = $con->prepare($query);
        $name = htmlspecialchars(strip_tags($_POST['nombre']));

        // bind the parameters
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':id', $id);
        var_dump($query);die;

        // prepare query for execution
        // posted values


        // Execute the query
        if ($stmt->execute()) {
            echo "<div class='alert alert-success'>Record was updated.</div>";
        } else {
            echo "<div class='alert alert-danger'>Unable to update record. Please try again.</div>";
        }

    } // show errors
    catch (PDOException $exception) {
        die('ERROR: ' . $exception->getMessage());
    }
}
